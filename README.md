# starlance_vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```


##Deploy on Ubuntu 18.04
Now, run this commands:
```
sudo apt install wget
```

```
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
```
```
source ~/.profile
```
Install 11.10.0
```
nvm install 11.10.0
```
Check version. node - 11.10.0 and npm 6.7.0

```
node -v
```

```
npm -v
```
Next, we need install all dependencies. Just run one by one

```
npm install -g @vue/cli
```

```
cd /home/starlance_vue/public_html
```

```
sudo git clone https://zelenchuk_sergei@bitbucket.org/zelenchuk_sergei/starlance_vue.git
```

```
cd starlance_vue
```
```
sudo npm i
```
If all commands finished with success status, build production version

```
sudo npm run build
```
After successful build command, change NGINX config for /dist directory. Enjoy :-)

## Errors
If not working sudo node -v, need to do
```
sudo chown -R $USER /home/starlance_vue/public_html/
```
or
```
nvm alias default node
```
This solving help use node and npm commands without sudo.

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
