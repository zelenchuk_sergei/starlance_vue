import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import About from './views/About.vue'
import Contact from './views/Contact.vue'
import CourseDetail from './views/CourseDetail.vue'

import Login from './views/Auth/Login.vue'
import Register from './views/Auth/Register.vue'
import ResetPassword from './views/Auth/ResetPassword.vue'

import PageNotFound from './views/PageNotFound.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },

        {
            path: '/about/',
            name: 'about',
            component: About
        },

        {
            path: '/contact/',
            name: 'contact',
            component: Contact
        },

        {
            path: '/courses/:slug/',
            name: 'CourseDetail',
            props: true,
            component: CourseDetail
        },

        {
            path: '/login/',
            name: 'Login',
            component: Login
        },

        {
            path: '/register/',
            name: 'Register',
            component: Register
        },

        {
            path: '/reset-password/',
            name: 'ResetPassword',
            component: ResetPassword
        },

        {
            path: '*',
            name: 'PageNotFound',
            component: PageNotFound
        },
    ]
})
