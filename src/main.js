import Vue from 'vue'
import vueHeadful from 'vue-headful'; /*https://github.com/troxler/vue-headful*/



import Vuetify from 'vuetify'
import App from './App.vue'
import router from './router'
import store from './store/store'

Vue.use(Vuetify)
Vue.config.productionTip = false

Vue.component('vue-headful', vueHeadful);

// index.js or main.js
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import 'material-design-icons-iconfont'

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
