import axios from 'axios'

const url = "https://api.starlance.com.ua/v1/"
// const url = "http://localhost/"

export default {
    state: {
        courses: [],
        program_titles: [],
    },
    mutations: {
        SET_COURSES(state, courses) {
            state.courses = courses
        },

        SET_TITLES(state, program_titles) {
            state.program_titles = program_titles
        },

    },
    actions: {
        async loadCourses({commit}) {
            commit("clearError")
            commit("setLoading", true)
            try {
                const response = await axios.get(url + 'courses/')
                //console.log(response.data.results)
                commit('SET_COURSES', response.data.results)
                commit("setLoading", false)
            } catch (error) {
                commit("setLoading", false)
                commit("setError", error)
                throw error
            }

        },

        async loadTitles({commit}, course_id) {
            commit("clearError")
            commit("setLoading", true)
            try {
                const response = await axios.get(url + 'program-titles-for-course-id/' + course_id + '/')

                /* the null needed in template rendering for showing program block */
                if (response.status !== 200 || response.data.count === 0) {
                    commit('SET_TITLES', null)
                } else {
                    commit('SET_TITLES', response.data.results)
                }
                // console.log(response)

                commit("setLoading", false)
            } catch (error) {
                commit("setLoading", false)
                commit('SET_TITLES', null)
                commit("setError", error)
                throw error
            }
        },

        async sendOrderToServer({commit}, data_obj) {
            commit("clearError")
            commit("setLoading", true)
            try {
                const response = await axios.post(url + "course-order/", data_obj)
                //console.log(response)
                if (response.status === 201) commit("setLoading", false)
            } catch (error) {
                commit("setLoading", false)
                commit("setError", error)
                throw error
            }
        },

    },
    // end actions

    getters: {
        courses(state) {
            return state.courses
        },

        program_titles(state) {
            return state.program_titles
        },

        coursesBySlug(state) {
            return courseSlug => {
                return state.courses.find(course => course.slug === courseSlug)
            }
        }
    }
    //end getters
}